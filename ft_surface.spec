# Copyright (c) 2023 Huawei Technologies Co., Ltd. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Name:           ft_surface
Version:        1.0.0
Release:        2
Summary:        fangtian engine surface wrapper for mesa

License:        Apache-2.0
URL:            https://gitee.com/openeuler/ft_surface
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  cmake g++

%description
ft_surface is wrapper for fangtian surface module in ft_engine. this lib is used for build mesa with ohos platform.

%package devel
Summary: ft_surface development files
Provides: ft_surface-devel

%description devel
This package contains necessary header files for ft_surface development.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%{cmake}
%cmake_build

%install
mkdir -p %{buildroot}%{_libdir}
cp -pdf %{__cmake_builddir}/libft_surface_wrapper.so %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp -pdf ft_surface_wrapper.pc %{buildroot}%{_libdir}/pkgconfig/

mkdir -p %{buildroot}%{_includedir}/ftsurface
cp -pdf include/buffer_handle.h %{buildroot}%{_includedir}/ftsurface/
cp -pdf include/display_type.h %{buildroot}%{_includedir}/ftsurface/
cp -pdf include/external_window.h %{buildroot}%{_includedir}/ftsurface/
cp -pdf include/native_window_wrapper.h %{buildroot}%{_includedir}/ftsurface/
cp -pdf include/window.h %{buildroot}%{_includedir}/ftsurface/

%files
%{_libdir}/*.so

%files devel
%{_libdir}/pkgconfig/ft_surface_wrapper.pc
%dir %{_includedir}/ftsurface
%{_includedir}/ftsurface/buffer_handle.h
%{_includedir}/ftsurface/display_type.h
%{_includedir}/ftsurface/external_window.h
%{_includedir}/ftsurface/native_window_wrapper.h
%{_includedir}/ftsurface/window.h

%changelog
* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 1.0.0-2
- adopt to new cmake macro

* Tue Oct 10 2023 xieyijun3 <xieyijun3@huawei.com> - 1.0.0-1
- init package
